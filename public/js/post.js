$( document ).ready(function() {

	$("#comment-new").click(function( event ){

		let post = $(event.currentTarget);
		let content = $('#comment_content').val();

		if (content === "") {
			alert('Cannot comment empty messages');
			return;
		}

		event.preventDefault();

		$.ajax({
			url: "/comment/post/"+post.data('post'),
			method: 'POST',
			data: {content:content}
		}).then(function(response) {
			$('#comment_content').val('');
			$('.comments-container').html(response);
		});
	});
});

function showComments(postId){

	$.ajax({
		url: "/comment/show/"+postId,
		method: 'GET',
		data: {content:content}
	}).then(function(response) {
		$('#comment_content').val('');
		$('.comments-container').html(response);
	});
}

function deleteComment(comment){

	$.ajax({
		url: "/comment/"+comment,
		method: 'DELETE'
	}).then(function(response) {
		$('#comment_content').val('');
		$('.comments-container').html(response);
	});
}

function hideComment(comment, visibility){
	$.ajax({
		url: "/comment/"+comment+'/edit',
		mimeType:"multipart/form-data",
		method: 'POST',
		data: {'visible': (visibility)}
	}).then(function(response) {
		$('#comment_content').val('');
		$('.comments-container').html(response);
	});
}



