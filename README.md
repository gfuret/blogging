# README #

Homework assignment

### Install repository ###

This repository was builded on laragon.

Steps:

Download repository to your www folder on laragon
Run composer install/update
Create "blogging" db on mysql server
Configure .env file "DATABASE_URL" for the specific configuration
Create tables with:
Run: php bin/console doctrine:migrations:migrate
Create some fake data with:
Run: php bin/console doctrine:fixtures:load
Site opnes
Open http://blogging.test/

### About ###

Simple blog app

Not registered users:

App main page is dashboard, this page lists all published posts from all users.

With a not logged user is possible to enter to all published post and add comments.

Registering as a user:

Main page is Your post, registered users can create posts and choose to publish them or not.

User owner of his post can edit and delete posts.

User can also delete or hide comments from any user.




### Explanations ###

Aside from the common symfony libraries, some other ones were added like:

Fixture: is fundamental to add and delete data in order to fully assess a proper QA
Faker: creates fake data and works perfectly with fixture
Security-bundle: access control and auth (it was also required)
web-profiler: debug symfony tool
