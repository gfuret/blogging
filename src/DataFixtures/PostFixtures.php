<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Post;
use App\Repository\ProductRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PostFixtures extends BaseFixtures implements DependentFixtureInterface
{
    private static $clickBaitParts = [
        '7 things to do', 'worst things to do', 'you will not imagine what she did', 'look what happened', 'imagine this'
    ];

    private static $postSubject = [
        'in Italy', 'at work', 'swimming', 'get in shape', 'sleeping cat', 'playful dog'
    ];

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Post::class, 5, function (Post $post, $count) {
            $post->setTitle($this->faker->randomElement(self::$clickBaitParts).' '.$this->faker->randomElement(self::$postSubject))
            ->setSlug($this->faker->slug)
            ->setCreatedAt(new \Datetime())
            ->setPublishedAt(new \Datetime())
            ->setPublished(true)
            ->setContent($this->faker->paragraph(3, true));
            $post->setUser($this->getReference(UserFixtures::POST_USER_REFERENCE));
        });
        $manager->flush();
    }
}
