<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixtures
{
    public const POST_USER_REFERENCE = 'post-user';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function loadData(ObjectManager $manager)
    {
        $userName = $this->faker->userName;
        $password = 'password';

        $user = new User();
        $user->setUsername($userName);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setRoles([User::ROLE_USER]);

        $manager->persist($user);
        $manager->flush();

        $this->addReference(self::POST_USER_REFERENCE, $user);
    }
}
