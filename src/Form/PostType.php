<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('slug', TextType::class, [
                'label'    => 'Slug or pretty url',
                'attr' => [
                    'placeholder' => 'example-of-a-well-done-slug',
                ]
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['class' => 'tinymce form-control'],
            ])
            ->add('published', CheckboxType::class, [
            'label'    => 'Publish immediately',
            'required' => false,
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
