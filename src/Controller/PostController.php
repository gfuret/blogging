<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/post")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="post_index", methods={"GET","POST"})
     */
    public function index(Request $request, PostRepository $postRepository, UserInterface $user = null): Response
    {
        if (is_null($user)) {
            throw new AccessDeniedException();
        }
        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->postSearch($request->get('search'), ['user' => $user->getId()]),
            'search' => $request->get('search'),
            'action' => "post_index"
        ]);
    }

    /**
     * @Route("/new", name="post_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserInterface $user = null): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        $post->setUser($user);
        $post->setCreatedAt(new \Datetime());
        if ($post->getPublished()) {
            $post->setPublishedAt(new \Datetime());
        }
        

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_index');
        }

        //$form->'attr' => array('checked'   => 'checked')

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}", name="post_show", methods={"GET"})
     */
    public function show(Post $post, CommentRepository  $commentRepository, UserInterface $user = null): Response
    {
        $form = $this->createForm(CommentType::class, (new Comment()));

        return $this->render(
            'post/show.html.twig',
            [
                'post' => $post,
                'form' => $form->createView(),
                'comments' => $post->getAllowedComments($user),
                'owner' => $post->isOwner($user)
        ]
        );
    }

    /**
     * @Route("/{slug}/edit", name="post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        
        if ($post->getPublished()) {
            $post->setPublishedAt(new \Datetime());
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Post $post): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_index');
    }
}
