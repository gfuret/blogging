<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
    * @Route("/post/{id}", name="comment_post", methods={"POST","GET"})
    */
    public function post(Post $post, Request $request, UserInterface $user = null)
    {
        if (empty($request->get('content'))) {
            return $this->render('comment/error.html.twig', [
                'error' => 'Could not complete the operation, try again or contact the administrators',
            ]);
        }

        $comment = new Comment();
        $comment->setUser($user);
        $comment->setPost($post);
        $comment->setVisible(true);
        $comment->setContent($request->get('content'));
        $comment->setCreatedAt(new \Datetime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->render('comment/index.html.twig', [
            'comments' => $post->getAllowedComments($user),
            'owner' => $post->isOwner($user),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Comment $comment, UserInterface $user): Response
    {
        $post = $comment->getPost();

        if ($user && $user->getId() == $post->getUser()->getId()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();

            return $this->render('comment/index.html.twig', [
                'comments' => $post->getAllowedComments($user),
                'owner' => true,
            ]);
        }

        return $this->render('comment/error.html.twig', [
                'error' => 'Could not complete the operation, try again or contact the administrators',
        ]);
    }

    /**
     * @Route("/new", name="comment_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $comment = new Comment();
        if (empty($comment->getPost())) {
            $comment->setUser(null);
        }
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('comment_index');
        }

        return $this->render('comment/new.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_show", methods={"GET"})
     */
    public function show(Post $post, UserInterface $user): Response
    {
        return $this->render('comment/index.html.twig', [
            'comments' => $post->getAllowedComments($user),
            'owner' => $post->isOwner($user),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comment_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Comment $comment, UserInterface $user = null): Response
    {
        $post = $comment->getPost();
        
        if (! $user || $user->getId() !== $post->getUser()->getId()) {
            return $this->render('comment/error.html.twig', [
                    'error' => 'Could not complete the operation, try again or contact the administrators',
            ]);
        }


        $this->editRequestHandler($request, $comment);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->render('comment/index.html.twig', [
                'comments' => $post->getAllowedComments(),
                'owner' => true,
            ]);
    }

    private function editRequestHandler(Request $request, Comment &$comment)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if (! is_null($request->request->get('visible'))) {
            $comment->setVisible(boolval($request->request->get('visible')));
        }
    }
}
