<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorController extends AbstractController
{
    /**
     * @Route("/error", name="error")
     */
    public function index(Request $request)
    {
        $requestException = $request->get('exception');

        $statusCode = (method_exists($requestException, 'getStatusCode')) ? $requestException->getStatusCode() : null;

        return $this->render('error/index.html.twig', [
            'status' => $statusCode,
            'message' => $requestException->getMessage(),
        ]);
    }

    public function __invoke(\Throwable $exception): Response
    {
        dd($this->errorRenderer);
        $exception = $this->errorRenderer->render($exception);
        return new Response($exception->getAsString(), $exception->getStatusCode(), $exception->getHeaders());
    }
}
