<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PostRepository;
use App\Entity\Post;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard", methods={"GET","POST"})
     */
    public function index(PostRepository $postRepository, Request $request)
    {
        return $this->render('dashboard/index.html.twig', [
            'posts' => $postRepository->postSearch($request->get('search'), ['published' => 1]),
            'search' => $request->get('search'),
            'action' => "dashboard"
        ]);
    }
}
